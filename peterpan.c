#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define STR_LIMIT_LENGTH 15

void init(char **matrix, int col_size, int str_length) {
	int mark_index;
	int last_col = col_size - 1;
	char mark;

	for (int i = 0; i < str_length; i++) {
		mark_index = (col_size - 1) * i + (col_size / 2);

		if (i % 3 == 2) {
			mark = '*';
		} else {
			mark = '#';
		}
		matrix[0][mark_index] = mark;
		matrix[last_col][mark_index] = mark;
	}
}

void handle_middleline(
		char *middle_line, char *upper_line, 
		int col_size, int row_size, char *str
	) {
	char mark1, mark2;
	int str_index = 0;

	for (int i = 1; i < row_size - 1; i++) {
		if (upper_line[i] == '#' || upper_line[i] == '*') {
			mark1 = mark2 = upper_line[i];

			if ((i+1) % (col_size - 1) == col_size / 2) {
				mark2 = str[str_index++];
			}

			if (middle_line[i-1] == '#' || middle_line[i-1] == '.') {
				middle_line[i - 1] = mark1;
			}
			middle_line[i + 1] = mark2;
		}
	}
}

int main(void) {
	char **matrix, mark;
	char str[STR_LIMIT_LENGTH];
	int col_size = 13, row_size, str_length, mirrorCol;

	printf("insert string : ");
	scanf("%s", str);

	str_length = strlen(str);

	// 대문자 검사
	for (int i=0; i < str_length; i++) {
		if (str[i] >= 0x61) return 0;
	}

	row_size = col_size + (col_size - 1) * (str_length - 1) + 1;
	matrix = malloc(sizeof(char *) * col_size);
	for (int i = 0; i < col_size; i++) {
		matrix[i] = malloc(sizeof(char) * row_size);
		memset(matrix[i], '.', sizeof(char) * row_size);
		matrix[i][row_size - 1] = '\0';
	}

	// 첫번째와 마지막 라인 초기값으로 생성
	init(matrix, col_size, str_length);

	// 첫번째 라인을 바탕으로 가지를 치듯이 마킹
	for (int j = 1; j < col_size / 2; j++) {
		mirrorCol = col_size - j - 1;
		for (int i = 0; i < row_size - 1; i++) {
			mark = matrix[j - 1][i];
			if (mark == '#' || mark == '*') {
				matrix[j][i - 1] = mark;
				matrix[j][i + 1] = mark;
				matrix[mirrorCol][i - 1] = mark;
				matrix[mirrorCol][i + 1] = mark;
			}
		}
	}

	// 조건문이 있는 가운데 라인은 따로 처리
	handle_middleline(matrix[col_size/2], matrix[col_size/2-1], col_size, row_size, str);

	for (int i = 0; i < col_size; i++) {
		printf("%s\n", matrix[i]);
		free(matrix[i]);
	}
	free(matrix);
	return 0;
}
